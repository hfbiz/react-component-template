import React from 'react';
import { DefaultButton } from '@fluentui/react';

var styles = {"test":"_styles-module__test__3ybTi"};

const DemoComponent1 = ({
  text
}) => {
  return /*#__PURE__*/React.createElement("div", {
    className: styles.test
  }, "DemoComponent1: ", text);
};

const DemoComponent2 = props => {
  const handleClick = () => {
    console.log('click');
  };

  return /*#__PURE__*/React.createElement(DefaultButton, {
    text: "DemoComponent2",
    iconProps: {
      iconName: 'Delete'
    },
    onClick: handleClick
  });
};

export { DemoComponent1, DemoComponent2 };
//# sourceMappingURL=index.modern.js.map
