function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));
var react = require('@fluentui/react');

var styles = {"test":"_styles-module__test__3ybTi"};

var DemoComponent1 = function DemoComponent1(_ref) {
  var text = _ref.text;
  return /*#__PURE__*/React.createElement("div", {
    className: styles.test
  }, "DemoComponent1: ", text);
};

var DemoComponent2 = function DemoComponent2(props) {
  var handleClick = function handleClick() {
    console.log('click');
  };

  return /*#__PURE__*/React.createElement(react.DefaultButton, {
    text: "DemoComponent2",
    iconProps: {
      iconName: 'Delete'
    },
    onClick: handleClick
  });
};

exports.DemoComponent1 = DemoComponent1;
exports.DemoComponent2 = DemoComponent2;
//# sourceMappingURL=index.js.map
