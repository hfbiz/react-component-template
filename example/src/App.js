
import React from 'react'
import { ThemeProvider } from '@fluentui/react-theme-provider';
import { initializeIcons } from '@fluentui/react/lib/Icons';

import { DemoComponent1, DemoComponent2 } from 'react-component-template'
import 'react-component-template/dist/index.css'

initializeIcons();

const theme = {};

const App = () => {
    return (
        <ThemeProvider applyTo="body" theme={theme}>
            <DemoComponent1 text="Create React Library Example 😄" />
            <DemoComponent2 />
        </ThemeProvider>
    );
}

export default App
