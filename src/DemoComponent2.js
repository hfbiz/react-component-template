
import React from 'react';
import { DefaultButton } from '@fluentui/react';

export const DemoComponent2 = (props) => {

    const handleClick = () => {
        console.log('click');
    }

    return (
        <DefaultButton
            text="DemoComponent2"
            iconProps={{iconName: 'Delete'}}
            onClick={handleClick}
        />
    );
};

