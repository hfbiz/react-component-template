import React from 'react'
import styles from './styles.module.css'

export const DemoComponent1 = ({ text }) => {
  return <div className={styles.test}>DemoComponent1: {text}</div>
}
