
# REACT-COMPONENT-TEMPLATE

## Create new project (library with component)

1. Create empty repository in Bitbucket (public) for library
2. Clone empty repository to local computer
2. Copy content of this template repository (without folder .git)
3. Rename in `package.json`
4. Rename `react-component-template` to `library` in `/example/package.json` and `example/src/App.js`
5. Update README.md for library
6. Commit and push to GIT repository with message "initial import"
7. Install dependencies for library and example (develop step 1)
8. Run library and example (develop step 2,3)

## Develop with Docker

1. Install dependencies
```
docker run -v ${PWD}:/app -w /app -it --rm node:14-alpine npm install
docker run -v ${PWD}:/app -w /app/example -it --rm node:14-alpine npm install
```

2. Run developing components (run in first terminal)
```
docker run -v ${PWD}:/app -w /app -it --rm node:14-alpine npm start
```

3. Run examples using components (run in second terminal)
```
docker run -v ${PWD}:/app -w /app/example -it --rm -p 3000:3000 node:14-alpine npm start
```

4. Run tests
```
docker run -v ${PWD}:/app -w /app/example -it --rm -p 3000:3000 node:14-alpine npm run test
```

5. Build production components
```
docker run -v ${PWD}:/app -w /app -it --rm node:14-alpine npm run build
```

## Develop in local development computer (without Docker)

1. Install dependencies
```
npm install
cd example
npm install
```

2. Run developing components (run in first terminal)
```
npm start
```

3. Run examples using components (run in second terminal)
```
cd example
npm start
```

4. Run tests
```
npm run test
```

5. Build production components
```
npm run build
```

## Release new version

1. Run test
2. Build production components
3. Commit and push to GIT repository with message "bump to version X.Y.Z"
4. Create and push GIT tag
